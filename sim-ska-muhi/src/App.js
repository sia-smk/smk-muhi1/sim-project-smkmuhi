import React from 'react';
import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Wellcome to <code>
            {/* src/App.js */}
            SKAMUHI Projects
            </code> and Happy Coding!.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Let Start to go to Mars!
        </a>
      </header>
    </div>
  );
}

export default App;
